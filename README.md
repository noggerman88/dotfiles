Uses GNU Stow for managing dotfiles
------------------------------------
Usage:
1. cd ~
2. git clone git@gitlab.com:turbonormie/dotfiles.git / git clone https://gitlab.com/turbonormie/dotfiles.git
3. stow -vt ~ */
------------------------------------
https://www.stevenrbaker.com/tech/managing-dotfiles-with-gnu-stow.html
https://www.gnu.org/software/stow/manual/stow.html

https://docs.gitlab.com/ee/user/ssh.html
