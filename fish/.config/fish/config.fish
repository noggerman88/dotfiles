####################
# Start X at login #
####################
#if status is-login
#    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
#        exec startx -- -keeptty
#    end
#end

if status --is-interactive

#############
# Variables #
#############
export QT_QPA_PLATFORMTHEME=qt5ct
export $(dbus-launch)
export WINEFSYNC=1
export PATH="/home/leon/bin:$PATH"
export PATH="/home/leon/AppImages:$PATH"
export PATH="/home/leon/.local/bin:$PATH"

#############
# Autostart #
#############
neofetch
end

############
# aliases  #
############

# Gentoo
alias full-upgrade="sudo emerge --ask --verbose --update --deep --newuse --with-bdeps=y --autounmask-write --autounmask-backtrack=y @world"

# Pcman
alias psy="sudo pacman c -Sy"
alias psyu="sudo pacman -Syu"

# GIT
alias glg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold green)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold yellow)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias ga="git add *"
alias gco="git commit -a -m"
alias gch="git checkout"
alias gs="git status"

alias mv="mv -i"
alias la="ls -alh"

alias svt="stow -vt ~ */"
